section .text

;terminates the process
exit:
	mov rax, 60			;num of syscall (exit)
	xor rdi, rdi
	ret
	
;takes a pointer to a null-terminated string and returns it's length
;need to save rax before call it
string_length:
    	xor rax, rax			;clean rax
    .count:
    	cmp byte [rdi+rax], 0		;check if the current symbol is null-terminator

    	je .end			;jump if we found null-terminator
    	inc rax			;go to next symbol and counter++
    	jmp .count
    	
    .end:
    	ret				;here rax should have return value
    	
;takes a pointer to a null-terminated string and prints it to stdout
;need to save rax before call it
print_string:
	push rdi			;saving rdi (callee-saved)
	push rsi			;saving rsi (callee-saved)
	
	push rax			;saving rax (caller-saved) 
    	call string_length
    	mov rdx, rax			;length - arg3
    	pop rax
    	
    	mov rsi, rdi			;arg2
    	mov rax, 1			;num of syscall (write)
    	mov rdi, 1			;descriptor (stdout)
    	syscall
    	
    	pop rsi
    	pop rdi
    	ret

;takes a character code and prints it to stdout
;need to save rax and rdx before call it
print_char:
	push rax			;saving rax (caller-saved)
	push rdi			;saving rdi (callee-saved)
    	
    	mov rdx, 1			;lenght(1 symbol)
    	
    	mov rdi, 1			;stdout
    	mov rsi, rsp
    	mov rax, 1
    				
    			
    	syscall
    	
    	pop rax
    	pop rdi
    	ret
    	
;line break
;use print_char
print_newline:
	
	push rdi			;saving rdi (callee-saved)
	
	mov rdi, 0xA			;char code
	
	push rax			;saving rax (caller-saved)
	push rdx			;saving rdx (caller-saved)
	call print_char
	pop rdx
	pop rax
	
	pop rdi
	ret
	
;prints an unsigned 8-byte number in decimal format
;need to save rax, rcx, rdx and r8 before call it
print_uint:
	push rdi			;saving rdi (callee-saved)
	mov rax, rdi			;arg1
	mov rcx, 10			;divider (for decimal format)
	mov rdi, rsp			;save stack state (callee-saved)
	sub rsp, 24
	dec rdi
	mov byte[rdi], 0
	
    .loop:
	xor rdx, rdx			;clean rdx
	div rcx
	add rdx, 48			;turn to ASCII (cause 48 is code of 0) 
	
	dec rdi
	mov [rdi], dl			;mod 
	
	test rax, rax			;check if 0	
    	jnz .loop			;do again (if not)
    	
    .end:
    	call print_string
    	
    	add rsp, 24			;turn the state of the stack back
    	pop rdi
    	ret
	
	
;prints a signed 8-byte number in decimal format
print_int:
	push rax			;saving rax (caller-saved)
	push rdx			;saving rdx (caller-saved)
	push rcx			;saving rcx (caller-saved)
	push r8			;saving r8 (caller-saved)
	push rdi			;saving rdi (callee-saved)
	
	cmp rdi, 0
	jge .print			;if above or equal
	mov rdi, 0x2D			;char ~ minus
	
	
	call print_char
	pop rdi
	push rdi
		
	neg rdi
	
    .print:
    	call print_uint
    	
    	pop rdi
    	pop r8
    	pop rcx
    	pop rdx
	pop rax
    	ret
    	
				
;takes two pointers to null-terminated strings and returns 1 (equal) or 0 (not equal)
;rdi - (first arg) pointer to string 1, rsi - (second arg) pointer to string 2
;need to save rax, rcx and r8 before call it
string_equals:
	xor rax, rax			;return value
	xor rcx, rcx 			;clean for loop (counter)
	xor r8, r8
	
    .loop:
    	mov r8b, byte [rdi+rcx]
    	cmp r8b, byte [rsi+rcx]	;check if equal
    	
    	jne .return			;if not equal - return 0
    	
    	cmp r8b, 0
    	je .equal	
    	
    	inc rcx
    	jmp .loop
    	
    .equal
    	mov rax, 1			;if equal - return 1
    	
    .return:
    	ret
    	

;reads one symbol from stdin and return it or 0 (if it's the end of the stream)
;need to save rdx before call it
read_char:
	push rdi			;saving rdi (callee-saved)
	push rsi			;saving rsi (callee-saved)
	
	push 0
	xor rax, rax			;num of syscall (read)
	xor rdi, rdi			;descriptor (stdin)
	mov rdx, 1			;length (one symbol)
	mov rsi, rsp			;arg (source index)
	syscall
	pop rax			;return value (0 or symbol)
	
	pop rsi
	pop rdi
	ret
	

;takes the addr of the start of the buffer and buffer's size
;reads a word from stdin into the buffer (skipping whitespace at the beginning)
;whitespace = 0x20, tab = 0x9, line break = 0xA
;stops and returns 0, if the word is too big for the buffer
;success = returns buffer's addr in rax, word's length in rdx
;no success = 0 in rax
;have to add a null-terminator to the word

;rdi - (first arg) addr of the start of the buffer
;rsi - (second arg) buffer's size

;need to save rax, rdx and rcx before call it
read_word:
	dec rsi
    .check_space_symbol:
    	call read_char
    	cmp rax, 0x20			;skip all these symbols
    	je .check_space_symbol
	cmp rax, 0x9
    	je .check_space_symbol
    	cmp rax, 0xA
    	je .check_space_symbol
    
    	xor rdx, rdx			;counter (keep length)
    .loop:
    	cmp rdx, rsi			;check if word is too big for the buffer
    	je .overflow
    	
    	test rax, rax			;check if 0 (have to add null-terminator)
    	jz .end
    	
    	mov [rdi+rdx], al		;al - 0..8 from rax (write to the buffer)
	inc rdx			;counter++

    	push rdx			;saving rdx (caller-saved)
    	call read_char
    	pop rdx
    	
    	cmp rax, 0x20			;skip all these symbols
    	je .end
	cmp rax, 0x9
    	je .end
    	cmp rax, 0xA
    	je .end
	
    	jmp .loop
    	
    .overflow:
    	xor rax, rax			;return 0 as exception
    	ret
    	
    .end:
    	mov byte [rdi+rdx], 0		;null-terminator
    	mov rax, rdi			;return value - addr (buffer)
    	ret
 
    
;takes a pointer to string
;tries to read from it unsigned number
;returns number in rax, length (num of symbols) in rdx
;rdx = 0, if no success in reading the number
;need to save rcx before call it
parse_uint:
	xor rdx, rdx			;counter (keep length)
	xor rax, rax
	xor rcx, rcx
    .loop:
    	mov cl, byte[rdi+rdx]
    	sub rcx, 48			;turn from ASCII to num
    	
    	cmp cl, 0			;check if it's a number
    	jl .end			;for signed cause ASCII
    	cmp cl, 9
    	jg .end
    	
    	inc rdx			;counter++
    	
    	imul rax, 10 			;left shift (for the next num)
    	add rax, rcx
    	jmp .loop
    	
   .end:
   	ret
    		

;takes a pointer to string
;tries to read from it signed number
;no space between sign and number
;returns number in rax, length (num of symbols, including sign) in rdx
;rdx = 0, if no success in reading the number
;need to save rax and rdx before call it
parse_int:
	cmp byte[rdi], '-'
	jne .unsigned
	
	inc rdi			;cause we need pointer on unsigned
	
	push rcx			;saving rcx (caller-saved)
	call parse_uint
	pop rcx
	
	test rdx, rdx
	je .end
	
	inc rdx			;increase length (cause of sign)
	
	neg rax
	dec rdi			;callee-saved
	ret
	
    .unsigned:
    	call parse_uint
    .end:
    	ret	

;takes a pointer to string, a pointer to buffer and buffer's length
;copies string into the buffer
;returns string's length if it's fits into the buffer or 0 if not

;rdi - (first arg) pointer to string
;rsi - (second arg) pointer to the buffer
;rdx - (third arg) buffer's size
string_copy:
	xor rcx, rcx			
	xor rax, rax
    .loop:
    	cmp rax, rdx			;check if too big for the buffer
    	je .overflow
    	
    	mov cl, byte [rax+rdi]
    	mov byte [rax+rsi], cl		;write to the buffer
    	cmp byte [rax+rsi], 0
    	je .end
    	inc rax
    	 
    	jmp .loop
    	
    .overflow:
    	xor rax, rax
    .end:
    	ret
	
	
	
	
				
	
	
	
	
	

	
